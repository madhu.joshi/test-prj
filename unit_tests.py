#!/usr/bin/env python3

import subprocess

ANSWER = b"Pick a number: Double the number: DDD\nSquare of the number: SSS\n"

for e in [0, 1, 2, 10, 134]:
        output = subprocess.check_output(f"echo {e} | node math_data.js",
                                         shell = True)
        answer = ANSWER.replace(b"DDD", str(2 * e).encode())
        answer = answer.replace(b"SSS", str(e ** 2).encode())
        result = output == answer
        print(f"Test result for {e}: {result}")
        if not result:
                open("ERRORS", "a").write(str(e) + "\n")
